process.env.BLUECAT_DEBUG_CONSOLE = true

var expect = require('chai').expect;
var Bluecat = require('Bluecat');
var Api = Bluecat.Api('yahooApi');

describe("Load first page of yahoo", function(done) {
	before(function() {
		t = new Bluecat.ServiceSync(Api, 'us.search.yahoo.com');
	})

	it("should return first yahoo search page", function(done) {
		t.run(function() {
			var r = t.search.GET({
				query : {
					fr : 'yhs-invalid',
					p : 'test'
				}
			});

			// verify response
			expect(r.err).to.equal(null);
			expect(r.data.statusCode).to.equal(200);
			//expect(r.data.body).to.have.ownProperty('specific');
			done();

		});
	});

});
